package co.com.almundo.callcenter;

import co.com.almundo.callcenter.colaboradores.*;
import co.com.almundo.callcenter.servicios.DispatcherImpl;

import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class DispatcherImplTest extends TestCase {

    private DispatcherImpl dispatcherImpl = new DispatcherImpl();

    private PriorityBlockingQueue<Empleado> empleados;
    private List<Llamada> llamadas;

    public void testWhenOneIncomingCall() {
        generarEmpleados(5,1,1);
        generarLlamadas(1);
        dispatcherImpl = new DispatcherImpl(llamadas.get(0), empleados);
        dispatcherImpl.dispatchCall();
        assert dispatcherImpl.getLlamada().getEmpleado() instanceof Operador;
    }

    public void testTresLlamadasDebenSerAtendidasPorTresOperadores() {
        try{
            generarEmpleados(3,2,1);
            Integer numeroLlamadas = 5;
            ExecutorService executorService = Executors.newFixedThreadPool(numeroLlamadas);
            generarLlamadas(numeroLlamadas);

            DispatcherImpl dispatcher1 = new DispatcherImpl(llamadas.get(0), empleados);
            DispatcherImpl dispatcher2 = new DispatcherImpl(llamadas.get(1), empleados);
            DispatcherImpl dispatcher3 = new DispatcherImpl(llamadas.get(2), empleados);

            executorService.execute(dispatcher1);
            executorService.execute(dispatcher2);
            executorService.execute(dispatcher3);

            Thread.sleep(10000);

            assert dispatcher1.getLlamada().getEmpleado() instanceof Operador;
            assert dispatcher2.getLlamada().getEmpleado() instanceof Operador;
            assert dispatcher3.getLlamada().getEmpleado() instanceof Operador;
        } catch (InterruptedException e) {
            e.printStackTrace();
            fail();
        }
    }

    public void testCincoLlamadasDebenSerAtendidasPorTresOperariosYDosSupervisores() {
        try{
            generarEmpleados(3,2,1);
            Integer numeroLlamadas = 6;
            ExecutorService executorService = Executors.newFixedThreadPool(numeroLlamadas);
            generarLlamadas(numeroLlamadas);

            DispatcherImpl dispatcher1 = new DispatcherImpl(llamadas.get(0), empleados);
            DispatcherImpl dispatcher2 = new DispatcherImpl(llamadas.get(1), empleados);
            DispatcherImpl dispatcher3 = new DispatcherImpl(llamadas.get(2), empleados);
            DispatcherImpl dispatcher4 = new DispatcherImpl(llamadas.get(3), empleados);
            DispatcherImpl dispatcher5 = new DispatcherImpl(llamadas.get(4), empleados);

            executorService.execute(dispatcher1);
            executorService.execute(dispatcher2);
            executorService.execute(dispatcher3);
            executorService.execute(dispatcher4);
            executorService.execute(dispatcher5);

            Thread.sleep(1000);

            List<Empleado> empleados = new ArrayList<>();

            empleados.add(dispatcher1.getLlamada().getEmpleado());
            empleados.add(dispatcher2.getLlamada().getEmpleado());
            empleados.add(dispatcher3.getLlamada().getEmpleado());
            empleados.add(dispatcher4.getLlamada().getEmpleado());
            empleados.add(dispatcher5.getLlamada().getEmpleado());

            assert dispatcher1.getLlamada().getEmpleado() instanceof Operador;
            assert dispatcher2.getLlamada().getEmpleado() instanceof Operador;
            assert dispatcher3.getLlamada().getEmpleado() instanceof Operador;
            assert dispatcher4.getLlamada().getEmpleado() instanceof Supervisor;
            assert dispatcher5.getLlamada().getEmpleado() instanceof Supervisor;

        } catch (InterruptedException e) {
            e.printStackTrace();
            fail();
        }
    }

    public void testSeisLlamadasDebenSerAtendidasPorTresOperariosDosSupervisoresYUnDirector() {
        try{
            generarEmpleados(3,2,1);
            Integer numeroLlamadas = 6;
            ExecutorService executorService = Executors.newFixedThreadPool(numeroLlamadas);
            generarLlamadas(numeroLlamadas);

            DispatcherImpl dispatcher1 = new DispatcherImpl(llamadas.get(0), empleados);
            DispatcherImpl dispatcher2 = new DispatcherImpl(llamadas.get(1), empleados);
            DispatcherImpl dispatcher3 = new DispatcherImpl(llamadas.get(2), empleados);
            DispatcherImpl dispatcher4 = new DispatcherImpl(llamadas.get(3), empleados);
            DispatcherImpl dispatcher5 = new DispatcherImpl(llamadas.get(4), empleados);
            DispatcherImpl dispatcher6 = new DispatcherImpl(llamadas.get(5), empleados);

            executorService.execute(dispatcher1);
            executorService.execute(dispatcher2);
            executorService.execute(dispatcher3);
            executorService.execute(dispatcher4);
            executorService.execute(dispatcher5);
            executorService.execute(dispatcher6);

            Thread.sleep(1000);

            List<Empleado> empleados = new ArrayList<>();

            empleados.add(dispatcher1.getLlamada().getEmpleado());
            empleados.add(dispatcher2.getLlamada().getEmpleado());
            empleados.add(dispatcher3.getLlamada().getEmpleado());
            empleados.add(dispatcher4.getLlamada().getEmpleado());
            empleados.add(dispatcher5.getLlamada().getEmpleado());
            empleados.add(dispatcher6.getLlamada().getEmpleado());

            assert dispatcher1.getLlamada().getEmpleado() instanceof Operador;
            assert dispatcher2.getLlamada().getEmpleado() instanceof Operador;
            assert dispatcher3.getLlamada().getEmpleado() instanceof Operador;
            assert dispatcher4.getLlamada().getEmpleado() instanceof Supervisor;
            assert dispatcher5.getLlamada().getEmpleado() instanceof Supervisor;
            assert dispatcher6.getLlamada().getEmpleado() instanceof Director;

        } catch (InterruptedException e) {
            e.printStackTrace();
            fail();
        }
    }

    public void testDiezLlamadasDebenSerAtendidasPorTresOperariosDosSupervisoresUnDirectorYLasDemasPorCualquierEmpleado() {
        try{
            generarEmpleados(3,2,1);
            Integer numeroLlamadas = 10;
            ExecutorService executorService = Executors.newFixedThreadPool(numeroLlamadas);
            generarLlamadas(numeroLlamadas);

            DispatcherImpl dispatcher1 = new DispatcherImpl(llamadas.get(0), empleados);
            DispatcherImpl dispatcher2 = new DispatcherImpl(llamadas.get(1), empleados);
            DispatcherImpl dispatcher3 = new DispatcherImpl(llamadas.get(2), empleados);
            DispatcherImpl dispatcher4 = new DispatcherImpl(llamadas.get(3), empleados);
            DispatcherImpl dispatcher5 = new DispatcherImpl(llamadas.get(4), empleados);
            DispatcherImpl dispatcher6 = new DispatcherImpl(llamadas.get(5), empleados);
            DispatcherImpl dispatcher7 = new DispatcherImpl(llamadas.get(6), empleados);
            DispatcherImpl dispatcher8 = new DispatcherImpl(llamadas.get(7), empleados);
            DispatcherImpl dispatcher9 = new DispatcherImpl(llamadas.get(8), empleados);
            DispatcherImpl dispatcher10 = new DispatcherImpl(llamadas.get(9), empleados);

            executorService.execute(dispatcher1);
            executorService.execute(dispatcher2);
            executorService.execute(dispatcher3);
            executorService.execute(dispatcher4);
            executorService.execute(dispatcher5);
            executorService.execute(dispatcher6);
            executorService.execute(dispatcher7);
            executorService.execute(dispatcher8);
            executorService.execute(dispatcher9);
            executorService.execute(dispatcher10);

            Thread.sleep(10000);

            List<Empleado> empleados = new ArrayList<>();

            empleados.add(dispatcher1.getLlamada().getEmpleado());
            empleados.add(dispatcher2.getLlamada().getEmpleado());
            empleados.add(dispatcher3.getLlamada().getEmpleado());
            empleados.add(dispatcher4.getLlamada().getEmpleado());
            empleados.add(dispatcher5.getLlamada().getEmpleado());
            empleados.add(dispatcher6.getLlamada().getEmpleado());
            empleados.add(dispatcher7.getLlamada().getEmpleado());
            empleados.add(dispatcher8.getLlamada().getEmpleado());
            empleados.add(dispatcher9.getLlamada().getEmpleado());
            empleados.add(dispatcher10.getLlamada().getEmpleado());

            assert dispatcher1.getLlamada().getEmpleado() instanceof Operador;
            assert dispatcher2.getLlamada().getEmpleado() instanceof Operador;
            assert dispatcher3.getLlamada().getEmpleado() instanceof Operador;
            assert dispatcher4.getLlamada().getEmpleado() instanceof Supervisor;
            assert dispatcher5.getLlamada().getEmpleado() instanceof Supervisor;
            assert dispatcher6.getLlamada().getEmpleado() instanceof Director;
            assert dispatcher7.getLlamada().getEmpleado() instanceof Empleado;
            assert dispatcher8.getLlamada().getEmpleado() instanceof Empleado;
            assert dispatcher9.getLlamada().getEmpleado() instanceof Empleado;
            assert dispatcher10.getLlamada().getEmpleado() instanceof Empleado;

        } catch (InterruptedException e) {
            e.printStackTrace();
            fail();
        }
    }

    public void generarLlamadas(Integer numeroLlamadas) {
        llamadas = new ArrayList<Llamada>();
        for (int i = 0; i < numeroLlamadas ; i++) {
            Llamada llamada = new Llamada();
            llamadas.add(llamada);
        }
    }

    public void generarEmpleados(Integer operadores, Integer supervisores, Integer directores){
        empleados = new PriorityBlockingQueue<Empleado>();

        for (int i = 0; i < operadores; i++) {
            Operador operador = new Operador(i, "Operador No. " + i, 1);
            empleados.add(operador);
        }

        for (int i = 0; i < supervisores; i++) {
            Supervisor supervisor = new Supervisor(i, "Supervisor No." + i, 2);
            empleados.add(supervisor);
        }

        for (int i = 0; i < directores; i++) {
            Director director = new Director(i, "Director No. " + i, 3);
            empleados.add(director);
        }
    }

}
