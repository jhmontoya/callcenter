import co.com.almundo.callcenter.colaboradores.*;
import co.com.almundo.callcenter.servicios.DispatcherImpl;
import sun.plugin.com.DispatchImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.PriorityBlockingQueue;

public class aplicacion {


    public static void main(String[] args) {
        new aplicacion().proceso();
    }

    private void proceso() {
        PriorityBlockingQueue<Empleado> empleados;
        List<Llamada> llamadas = new ArrayList<>();
        empleados = new PriorityBlockingQueue<>(  );

        for (int i = 0; i < 5 ; i++) {
            Operador operador = new Operador( i, "Operador No. " + i, 1);
            empleados.add( operador );
        }

        for (int i = 0; i < 3 ; i++) {
            Supervisor supervisor = new Supervisor( i, "Supervisor No. " + i, 1);
            empleados.add( supervisor );
        }

        for (int i = 0; i < 2 ; i++) {
            Director director = new Director( i, "Director No. " + i, 1);
            empleados.add( director );
        }

        int numeroDellamadas = 40;

        for (int i = 0; i < numeroDellamadas; i++) {
            Llamada llamada = new Llamada();
            llamadas.add(llamada);
        }

        ExecutorService executorService = Executors.newFixedThreadPool( numeroDellamadas );

        llamadas.stream()
                .map(llamada -> new DispatcherImpl(llamada, empleados))
                .forEach(executorService :: execute);

        executorService.shutdown();
    }

}
