package co.com.almundo.callcenter.colaboradores;

public class Empleado implements Comparable<Empleado> {
    private Integer identificacion;
    private String nombre;
    private Integer prioridad;

    public Empleado(Integer id, String nombre, Integer prioridad) {
        this.identificacion = id;
        this.nombre = nombre;
        this.prioridad = prioridad;
    }

    public Integer getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(Integer identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(Integer prioridad) {
        this.prioridad = prioridad;
    }

    public int compareTo(Empleado empleado) {
        return this.prioridad.compareTo( empleado.getPrioridad() );
    }
}
