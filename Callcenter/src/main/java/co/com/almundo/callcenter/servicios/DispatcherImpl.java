package co.com.almundo.callcenter.servicios;

import co.com.almundo.callcenter.colaboradores.Empleado;
import co.com.almundo.callcenter.colaboradores.Llamada;

import java.util.Objects;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.logging.Level;

import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;

public class DispatcherImpl implements Runnable, IDispatcher {

    private Llamada llamada;
    private PriorityBlockingQueue<Empleado> empleados;

    public DispatcherImpl(){

    }
    public DispatcherImpl(Llamada llamada, PriorityBlockingQueue<Empleado> empleados) {
        this.llamada = llamada;
        this.empleados = empleados;
    }

    public Llamada getLlamada() {
        return llamada;
    }

    public void setLlamada(Llamada llamada) {
        this.llamada = llamada;
    }

    public PriorityBlockingQueue<Empleado> getEmpleados() {
        return empleados;
    }

    public void setEmpleados(PriorityBlockingQueue<Empleado> empleados) {
        this.empleados = empleados;
    }

    public void run() {
        dispatchCall();
    }

    public void dispatchCall() {
        try {
        Empleado empleado = empleados.poll();
            if(Objects.isNull(empleado)) {
                LOGGER.info("Llamada "+llamada.getIdLlamada()+" en espera.");
                empleado = empleados.take();
            }
            llamada.setEmpleado(empleado);
            LOGGER.info("El empleado "+ empleado.getNombre()+" con identificacion "+empleado.getIdentificacion()+
                    " tomo la llamada "+llamada.getIdLlamada());
            Thread.sleep(llamada.getDuracionLlamada().longValue());
            LOGGER.info("Llamada "+llamada.getIdLlamada()+" ha finalizado.");
            empleados.add(empleado);
        } catch (InterruptedException e) {
            LOGGER.log(Level.WARNING, "Error al atender la llamada "+llamada.getIdLlamada(), e);
        }
    }
}
