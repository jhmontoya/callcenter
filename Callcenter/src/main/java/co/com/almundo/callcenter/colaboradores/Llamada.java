package co.com.almundo.callcenter.colaboradores;

import java.util.Random;

public class Llamada {

    private Float duracionLlamada;
    private String idLlamada;
    private Empleado empleado;

    public Llamada() {
        this.duracionLlamada = new Random().nextFloat() * (5000 - 1000) + 5000;
        this.idLlamada = java.util.UUID.randomUUID().toString();
    }

    public Float getDuracionLlamada() {
        return duracionLlamada;
    }

    public void setDuracionLlamada(Float duracionLlamada) {
        this.duracionLlamada = duracionLlamada;
    }

    public String getIdLlamada() {
        return idLlamada;
    }

    public void setIdLlamada(String idLlamada) {
        this.idLlamada = idLlamada;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }
}
