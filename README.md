# callCenter

##Solucion

1. Se creo una clase Empleado la cual contiene la informacion basica de la persona.
2. Se extiende la clase empleado a los diferentes roles que van a interactura en el callcenter.
3. Se crea la clase Llamada para almacenar el tiempo para almacenar el empleado que atendera la llamada, y el tiempo de la misma.
4. Para dar solucion a la asignacion de las llamadas por prioridad, se crea una lista de tipo PriorityBlockingQueue, 
la cual es una cola FIFO que permite ordenar los empleados por prioridad ( Se implementa la interface Comparable en Empleado 
para establecer el atributo con el que se ordenara).
5. Para cuando no hay empleados libres en la cola, se valida inicialmente si definitivamente no hay empleados libres 
utilizando el metodo poll, si este retorna null, se realiza el bloqueo de la cola por medio del método take() hasta que 
haya un empleado disponible. 
6. Para tener llamadas concurrentes se implementó la interface Runnable en la clase DistpacherImpl, que unido a la 
implementación de cola concurrente permite controlar de forma acertada cualquier cantidad de llamadas entrante.


